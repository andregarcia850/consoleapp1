﻿// See https://aka.ms/new-console-template for more information
//Console.WriteLine("Hello, World!" + args[0]);

using System.Net.Http.Headers;

Console.WriteLine("Reading file: " + args[0]);
string[] lines = System.IO.File.ReadAllLines(args[0]);


bool shouldExecute = true;
String lastExecutionFile = "last_execution.txt";
string[] executionTimes;
try
{
    executionTimes = System.IO.File.ReadAllLines(lastExecutionFile);
    if (executionTimes.Length > 0 && executionTimes[^1].Trim().Length > 0)
    {
        DateTime parsedDate = DateTime.Parse(executionTimes[^1]);
        if ((DateTime.UtcNow - parsedDate) < new TimeSpan(0, 1, 0))
        {
            shouldExecute = false;
        }
    }
} catch(Exception ex){

}

if (!shouldExecute)
{
    Console.WriteLine("Skipping execution because last execution was less than 60 seconds ago");
}
else
{
    HttpClient client = new();
    client.DefaultRequestHeaders.Accept.Clear();
    client.DefaultRequestHeaders.Accept.Add(
        new MediaTypeWithQualityHeaderValue("application/json"));
    client.DefaultRequestHeaders.Add("User-Agent", ".NET Foundation Repository Reporter");

    String outputFileName = "output.log";
    StreamWriter outputFile = new(outputFileName);

    foreach (string user in lines)
    {
        string url = "https://api.bitbucket.org/2.0/users/" + user;
        Console.WriteLine("Fetching information for user=" + user + " url=" + url);

        Task<HttpResponseMessage> getAsyncTask = client.GetAsync(url);
        

        try
        {
            HttpResponseMessage response = getAsyncTask.Result;
            string content = new StreamReader(response.Content.ReadAsStream()).ReadToEnd();
            Console.WriteLine("result=" + content + " statusCode=" + response.StatusCode);
            outputFile.WriteLine(content);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex);
        }
        Thread.Sleep(5000);
    }
    try
    {
        System.IO.File.AppendAllText(lastExecutionFile, DateTime.UtcNow.ToString() + "\r\n");
    }
    catch (Exception ex)
    {

    }
    outputFile.Flush();
    outputFile.Close();
}


