# README #

Exercise implemented in C#.  
  
Reads input from file provided path to console application. Input file contains one user for each line. Reads from bitbucket API information regarding the user.  Prints the result to the console and writes to output file. There is a pause between each request and a pause after the last request. The console app will not execute if it has been executed in the last 60 seconds.
